//------------------------------------------------------------------------------
// Copyright (c) 2022, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: example_of_finding_the_caller_of_my_caller
// Example of how to find the process that called the process that called me
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define process_information_pseudo_filesystem	"/proc/"
#define my_process_directory					"self"
#define link_to_the_executable_of_this_process	"/exe"
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void error( const char *format, ... ) {
	va_list argp;
	char buff[256];
	va_start(argp, format);
	vsprintf(buff, format, argp);
	va_end(argp);
	fprintf(stderr, "ERROR: %s\n", buff);
	fflush(stderr);
	exit(EXIT_FAILURE);
}

static void print( const char *label, char *value) {
	fprintf(stdout, "%-31s : %s\n", label, value);
}

static ssize_t getexecname( char *exec, char *path, size_t len) {
	ssize_t cc = readlink(exec, path, len);
	path[cc] = '\0';
	return cc;
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main() {
	char pathToLook[PATH_MAX];
	char pathFound[PATH_MAX];

	sprintf(pathToLook, "%s%s%s", process_information_pseudo_filesystem, my_process_directory, link_to_the_executable_of_this_process);
	if (getexecname(pathToLook, pathFound, sizeof(pathFound)) == -1)
		error("Cannot get name of the current executable.", "");
	print("Current process", pathFound);
	//---- Caller
	pid_t caller = getppid();
	sprintf(pathToLook, "%s%d%s", process_information_pseudo_filesystem, caller, link_to_the_executable_of_this_process);
	if (getexecname(pathToLook, pathFound, sizeof(pathFound)) == -1)
		error("Cannot get name of the process %d.", caller);
	print("Calling process", pathFound);
	//---- Caller's caller
	char command[PATH_MAX];
	sprintf(pathToLook, "ps --no-headers -f %d", caller);
	FILE *fd = popen(pathToLook, "r");
	if (fd == NULL)
		error("Cannot open command: %s", pathToLook);
	if (NULL == fgets(pathToLook, sizeof(pathToLook), fd))
		error("Cannot read command: %s", pathToLook);
	pclose(fd);
	sscanf(pathToLook, "%s%s%d", pathFound, pathFound, &caller);
	sprintf(pathToLook, "ps --no-headers -o command %d", caller);
	fd = popen(pathToLook, "r");
	if (fd == NULL)
		error("Cannot open command: %s", pathToLook);
	if (NULL == fgets(command, sizeof(command), fd))
		error("Cannot read command: %s", pathToLook);
	pclose(fd);
	if (command[strlen(command) - 1] == '\n')command[strlen(command) - 1] = '\0';
	print("Calling process calling process", command);
	//---- Exit ----------------------------------------------------------------
	return EXIT_SUCCESS;
}
//------------------------------------------------------------------------------
