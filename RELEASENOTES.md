# RELEASE NOTES: *example_of_finding_the_caller_of_my_caller*, Example of how to find the process that called the process that called me.

Functional limitations, if any, of this version are described in the *README.md* file.

**Version 1.0.1**:
  - Removed unused make file.

**Version 1.0.0**:
  - First version.
