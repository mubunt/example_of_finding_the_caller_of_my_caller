 # *example_of_finding_the_caller_of_my_caller*, Example of how to find the process that called the process that called me.

## LICENSE
**example_of_finding_the_caller_of_my_caller** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE
``` bash
$ ./linux/example_of_finding_the_caller_of_my_caller
Current process                 : /home/michel/Sandbox/GitLab/example_of_finding_the_caller_of_my_caller/linux/example_of_finding_the_caller_of_my_caller
Calling process                 : /usr/bin/bash
Calling process calling process : /usr/bin/xfce4-terminal

```
## STRUCTURE OF THE APPLICATION
This section walks you through **example_of_finding_the_caller_of_my_caller**'s structure. Once you understand this structure, you will easily find your way around in **example_of_finding_the_caller_of_my_caller**'s code base.

``` bash
$ yaTree
./                                                   # Application level
├── src/                                             # Source directory
│   ├── Makefile                                     # Makefile
│   └── example_of_finding_the_caller_of_my_caller.c # Example C source file
├── COPYING.md                                       # GNU General Public License markdown file
├── LICENSE.md                                       # License markdown file
├── Makefile                                         # Makefile
├── README.md                                        # ReadMe markdown file
├── RELEASENOTES.md                                  # Release Notes markdown file
└── VERSION                                          # Version identification text file

1 directories, 8 files
$
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd example_of_finding_the_caller_of_my_caller
$ make clean all
```
## SOFTWARE REQUIREMENTS
- For usage and development , nothing particular...
- Developped and tested on XUBUNTU 22.04, GCC version 11.2.0 (Ubuntu 11.2.0-19ubuntu1), GNU Make 4.3

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***